# Multiagent cooperative reinforcement learning
### An experiment of agents working together to solve a challenge


This repository contains a simple example of artificial agents working in unison to accomplish a task. In this case, the task is playing PettingZoo's [Knights Archers Zombies](https://www.pettingzoo.ml/butterfly/knights_archers_zombies), a Pygame-made game that pits a certain amount of archers and knights against a horde of zombies. Much of the code is repurposed from, and inspired by, PettingZoo's excellent [pistonball tutorial with RLLib](https://github.com/Farama-Foundation/PettingZoo/blob/master/tutorials/rllib_pistonball.py).

## Technical details

Like any experiment that deals with more than one RL agent, this one must be fine-tuned extensively for the best results to be achieved. Ray's RLLib and Tune libraries are perfect for this, as they allow for a high degree of customization before any computation takes place. The environment itself is part of PettingZoo, a Python library designed specifically to handle multiagent reinforcement learning experiments.

A balance has been reached with the current configuration, but it can certainly be improved for more powerful systems. Proximal Policy Optimization (PPO) and a convolutional neural network with 3 convolutions seem to perform very well. Though the algorithm required roughly 5 million timesteps to achieve optimal results, an evolution can be seen from > 10k timesteps, where the archers and knights not only start moving around more efficiently, but also become more capable of killing zombies.

## Installation and usage

Instead of having to install every library by hand, you can navigate to the cloned folder and use `pip install -r requirements.txt` to install everything automatically. It is, however, highly recommended that you do this in a virtual environment to avoid breaking any existing packages.

If you only want to run the algorithm to see how it performs on pre-trained data, simply launch `main.py` and edit the path variables so that they contain the `ray_results` folder; otherwise, use `train.py` to train the agents and then launch `main.py`. The simulation's results are saved as a GIF ("out.gif") file.