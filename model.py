# Neural network model used for the RL agents
# Makes use of a standard convolutional neural network for visual input processing

from torch import nn
from ray.rllib.models.torch.torch_modelv2 import TorchModelV2

class CNN(TorchModelV2, nn.Module):
    def __init__(self, obs_space, act_space, num_outputs, *args, **kwargs):
        TorchModelV2.__init__(self, obs_space, act_space, num_outputs, *args, **kwargs)
        nn.Module.__init__(self)
        self.model = nn.Sequential(
            nn.Conv2d(3, 32, [8, 8], stride=(4, 4)),
            nn.ReLU(),
            nn.Conv2d(32, 64, [4, 4], stride=(2, 2)),
            nn.ReLU(),
            nn.Conv2d(64, 64, [3, 3], stride=(1, 1)),
            nn.ReLU(),
            nn.Flatten(),
            nn.Linear(3136, 512),
            nn.ReLU()
        )
        self.policy_fn = nn.Linear(512, num_outputs)
        self.value_fn = nn.Linear(512, 1)

    def forward(self, input_dict, state, seq_lens):
        out = self.model(input_dict["obs"].permute(0, 3, 1, 2))
        self._value_out = self.value_fn(out)

        return self.policy_fn(out), state

    def value_function(self):
        return self._value_out.flatten()