# Main (runner) file

import os
import ray
from ray import tune
from ray.tune.registry import register_env
from ray.rllib.env.wrappers.pettingzoo_env import ParallelPettingZooEnv
from ray.rllib.models import ModelCatalog
from model import *
from environment import *

ray.init(object_store_memory=78643200)

if __name__ == "__main__":
    env_name = "knights_archers_zombies_v7"

    register_env(env_name, lambda config: ParallelPettingZooEnv(env_creator(config)))

    env = ParallelPettingZooEnv(env_creator({}))
    obs_space = env.observation_space
    act_space = env.action_space

    ModelCatalog.register_custom_model("CNNV1", CNN)

    def gen_policy(i):
        config = {
            "model": {
                "custom_model": "CNNV1",
            },
            "gamma": 0.99,
        }
        return (None, obs_space, act_space, config)

    policies = {"policy_0": gen_policy(0)}
    policy_ids = list(policies.keys())

    config = {
        # Environment specific
        "env": env_name,
        # General
        "log_level": "ERROR",
        "framework": "torch",
        "num_gpus": int(os.environ.get("RLLIB_NUM_GPUS", "0")),
        "num_workers": 0,
        "num_envs_per_worker": 1,
        "compress_observations": False,
        "batch_mode": "truncate_episodes",
        "use_gae": True,
        "lambda": 0.9,
        "gamma": .99,
        "clip_param": 0.1,
        "grad_clip": None,
        "entropy_coeff": 0.1,
        "vf_loss_coeff": 0.25,
        "sgd_minibatch_size": 64,
        "num_sgd_iter": 4, 
        "rollout_fragment_length": 512,
        "train_batch_size": 512 * 4,
        "lr": 2e-05,
        "clip_actions": True,

        # Method specific
        "multiagent": {
            "policies": policies,
            "policy_mapping_fn": (
                lambda agent_id: policy_ids[0]),
        },
    }

    tune.run(
        "PPO",
        name="PPO",
        stop={"timesteps_total": 5000000}, 
        checkpoint_freq=10,
        checkpoint_at_end=True,
        local_dir="../multiagent-coop-rl/ray_results/" + env_name,
        config=config,
        restore="../multiagent-coop-rl/ray_results/knights_archers_zombies_v7/PPO/PPO_knights_archers_zombies_v7_118b3_00000_0_2021-11-09_09-17-56/checkpoint_000560/checkpoint-560"
    )