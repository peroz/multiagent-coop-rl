# Setting up the Knights Archers and Zombies ("KAZ") environment

from pettingzoo.butterfly import knights_archers_zombies_v7
import supersuit as ss

def env_creator(args):
    env = knights_archers_zombies_v7.parallel_env(
        spawn_rate=20, 
        num_knights=2, 
        num_archers=2,
        killable_knights=True, 
        killable_archers=True, 
        line_death=True, 
        pad_observation=True,
        max_cycles=900
    )
    env = ss.color_reduction_v0(env, mode='B')
    env = ss.dtype_v0(env, "float32")
    env = ss.resize_v0(env, x_size=84, y_size=84)
    env = ss.frame_stack_v1(env, 3)
    env = ss.normalize_obs_v0(env, env_min=0, env_max=1)
    
    return env