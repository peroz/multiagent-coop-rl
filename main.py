# Main (runner) file

import os
import pickle
import ray
from ray.tune.registry import register_env
from ray.rllib.agents.ppo import PPOTrainer
from ray.rllib.env.wrappers.pettingzoo_env import PettingZooEnv
from ray.rllib.models import ModelCatalog
from model import *
from environment import *
import PIL

os.environ["SDL_VIDEODRIVER"] = "dummy"

path = "../multiagent-coop-rl/ray_results/knights_archers_zombies_v7/PPO/"
current_exp = path + "PPO_knights_archers_zombies_v7_118b3_00000_0_2021-11-09_09-17-56/"
checkpoint_path = current_exp + "checkpoint_000560/checkpoint-560"
params_path = current_exp + "params.pkl"

ModelCatalog.register_custom_model("CNNV1", CNN)

def env_creator():
    env = knights_archers_zombies_v7.env(
        spawn_rate=20, 
        num_knights=2, 
        num_archers=2,
        killable_knights=True, 
        killable_archers=True, 
        line_death=True, 
        pad_observation=True,
        max_cycles=900
    )
    env = ss.color_reduction_v0(env, mode='B')
    env = ss.dtype_v0(env, "float32")
    env = ss.resize_v0(env, x_size=84, y_size=84)
    env = ss.frame_stack_v1(env, 3)
    env = ss.normalize_obs_v0(env, env_min=0, env_max=1)
    
    return env

env = env_creator()
env_name = "knights_archers_zombies_v7"
register_env(env_name, lambda config: PettingZooEnv(env_creator()))

with open(params_path, "rb") as f:
    config = pickle.load(f)

ray.init(num_cpus=1, num_gpus=0)
PPOagent = PPOTrainer(env=env_name, config=config)
PPOagent.restore(checkpoint_path)

reward_sum = 0
frame_list = []
i = 0
env.reset()

for agent in env.agent_iter():
    observation, reward, done, info = env.last()
    reward_sum += reward

    if done:
        action = None
    else:
        action, _, _ = PPOagent.get_policy("policy_0").compute_single_action(observation)

    env.step(action)
    i += 1
    
    if i % (len(env.possible_agents) + 1) == 0:
        frame_list.append(PIL.Image.fromarray(env.render(mode="rgb_array")))

env.close()

print(reward_sum)

frame_list[0].save("out.gif", save_all=True, append_images=frame_list[1:], duration=100, loop=0)